define [
  'underscore'
  'backbone'
  'models/Default-model'
], (_, Backbone, LoginModel) ->

  class DefaultCollection extends Backbone.Collection
    model: DefaultModel