define [
  'underscore'
  'backbone'
  'models/Login-model'
], (_, Backbone, LoginModel) ->

  class LoginCollection extends Backbone.Collection
    model: LoginModel