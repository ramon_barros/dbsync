define [
  'backbone'
  'views/login'
], (Backbone, LoginView) ->
  class LoginRouter extends Backbone.Router
    showLogin: ->
        console.log "LoginRouter"
        new LoginView()
    routes: {
        "login": "showLogin"
    }
  new LoginRouter()