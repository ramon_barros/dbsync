define [
  'backbone'
  'views/default'
], (Backbone, DefaultView) ->
  class DefaultRouter extends Backbone.Router
    showDefault: ->
        console.log "DefaultRouter"
        new DefaultView()
    routes: {
        "*action":"showDefault"
    }
  new DefaultRouter()