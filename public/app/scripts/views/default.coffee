define [
  'jquery'
  'underscore'
  'backbone'
  'templates'
], ($, _, Backbone, JST) ->
  class DefaultView extends Backbone.View
    el: ".content"
    template: JST['app/scripts/templates/default.ejs']
    initialize: ->
        console.log("DefaultView:initialize")
        @render()
    render: ->
        console.log("DefaultView:render")
        $(@el).html(@template)
        #$(".jumbotron").slideDown()
        #$(".jumbotron").removeClass("hidden")