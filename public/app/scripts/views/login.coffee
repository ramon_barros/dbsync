define [
  'jquery'
  'underscore'
  'backbone'
  'templates'
], ($, _, Backbone, JST) ->
  class LoginView extends Backbone.View
    el: ".content"
    template: JST['app/scripts/templates/login.ejs']
    initialize: ->
        console.log("LoginView:initialize")
        @render()
    render: ->
        console.log("LoginView:render")
        $(@el).html(@template)
        #$(".jumbotron").slideDown()
        #$(".jumbotron").removeClass("hidden")