define [
  'underscore'
  'backbone'
], (_, Backbone) ->
  'use strict';

  class LoginModel extends Backbone.Model