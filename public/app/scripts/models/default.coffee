define [
  'underscore'
  'backbone'
], (_, Backbone) ->
  'use strict';

  class DefaultModel extends Backbone.Model